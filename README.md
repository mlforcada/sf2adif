# sf2adif

`sfadif` may be used to convert a variety of user-defined *short-format* ham radio logs into standard ADIF files.


## Installation
Make sure GNU Awk (gawk) is installed in your system. `sf2adif` has been tested with `gawk` 5.2.1, but it may work well with earlier and later versions. It does not work with `mawk`.

## Usage
The AWK program `sf2adif` is invoked through a command-line interface. Drop `sf2adif.awk` in a directory, drop the short-format file to be processed there (say, `mylog.sf`), start the command-line interface, navigate to that directory, and type 
```
gawk -f sf2adif.awk mylog.sf >mylog.adi
```
to produce an ADIF file `mylog.adi` from your file.

## File format

Input to `sf2adif` may consist of one or more files, which will be concatenated.

Files are processed line by line. Lines in these files may be of four kinds: *definition* lines, *content* lines, *change* lines, *append* lines, *chop-off* lines, and comments.

### Definition lines and content lines
There are four kinds of definition lines, and three kinds of content lines.

#### Log-level definition lines
*Log-level* definition lines define the format of *log-level content lines* and start with `&` and a regular expression. For instance, the definition
```
& [0-9]{8} QSO_DATE STATION_CALLSIGN OPERATOR MY_SOTA_REF MY_GRIDSQUARE
``` 
uses the regular expression `[0-9]{8}` after the `&` to recognize the 8-digit date that will start log-level content lines such as 
```
20231118 EA5IYL EA5IYL EA5/CS-012 IN90vi Castell de Culla
```
According to the definition, the value `20231118` will be assigned to ADIF field `QSO_DATE`, the value `EA5IYL` to ADIF field `STATION_CALLSIGN`, the value `EA5IYL` to ADIF field `OPERATOR`, the value `EA5/CS-012` to the ADIF field `MY_SOTA_REF`, the value `IN90vi` to the ADIF field `MY_GRIDSQUARE` and the trailing text up to the end of the line `Castell de Culla` will be added to the first part of ADIF fields `COMMENT` and `NOTES` for all the QSOs in the log.

#### Section-level definition lines
*Section-level* definition lines define the format of *section-level content lines* and start with `@` and a regular expression (a log may contain different sections). For instance, the definition
```
@ (FM|SSB|USB|LSB|CW) MODE FREQ TX_PWR 
```
uses the regular expression `(FM|SSB|USB|LSB|CW)` to recognize the mode name that will start section-level content lines such as
```
CW 14.062 20 Xiegu G90 + 20-m-band whip + counterpoise
```
According to the definition, the value `CW` will be assigned to ADIF field `MODE`, the value `14.062` to ADIF field `FREQ`, the value `20` to ADIF field `TX_PWR`, and the trailing text up to the end of the line `Xiegu G90 + 20-m-band whip + counterpoise` will be added to the second part of ADIF fields `COMMENT` and `NOTES` for all the QSOs in the log.

#### QSO definition lines
*QSO* definition lines define the format of *QSO content lines* and start with `=` and a regular expression (a section contains a number of QSOs). For instance, the definition
```
= [0-9]{4} TIME_ON CALL RST_SENT RST_RCVD
```
uses the regular expression `[0-9]{4}` to recognize the 4-digit time that will start QSO content lines such as
```
1110 IK2LEY/P 579 559 S2S:I/LO-422
```
According to the definition, the value `1110` will be assigned to ADIF field `TIME_ON`, the value `IK2LEY/P` to ADIF field `CALL`, the value `579` to ADIF field `RST_SENT`, the value `559` to ADIF field `RST_RCVD` and the trailing text up to the end of the line `S2S:I/LO-422` will be added to the end of ADIF fields `COMMENT` and `NOTES` for this particular QSO.

When QSO lines are processed, ADIF QSO records are written and finished with `<EOR>`.

#### Adding fields that will be discarded to `COMMENT` and  `NOTES`

Definitions starting with `+`, such as
```
+ freq= FREQ out= RST_SENT in= RST_RCVD sota_ref= MY_SOTA_REF
```
are optional, and extend ADIF fields `COMMENT` and `NOTES` for a QSO with (redundant) information present in the ADIF fields mentioned, introduced with the strings provided; for instance, this would add
```
freq=14.062 out=579 in=599 sota_ref=EA5/CS-012
```
to these fields. In this way, comments can be used to record the information in ADIF fields that are discarded by a specific ADIF log processor (for instance, `RST_SENT` and `RST_RCVD` are discarded by SOTAData when logs are uploaded).

### Change lines
Lines starting with `>` may be used to modify in situ a specific section-level field for the remaining QSOs in the section. For instance, the line
```
> FREQ 14.286
```
changes ADIF field `FREQ` to `14.286`. This may be useful, for instance, when frequency-hopping.

### Append lines
Lines starting with `(` may be used to add a field in situ for the following QSOs in the section. For example, the line 
```
( SOTA_REF
```
will change the format of QSO lines to include an additional ADIF field `SOTA_REF` as the last item. This, in effect, would be equivalent to inserting a new QSO definition line of the form
```
= [0-9]{4} TIME_ON CALL RST_SENT RST_RCVD SOTA_REF
```
so that a QSO line could now have the form
```
1110 IK2LEY/P 579 559 I/LO-422 Nice QSO
```
and the resulting ADIF record would contain now 
```
<SOTA_REF:8>I/LO-422
```

### Chop-off lines
The last field added to the QSO line definition by an *append* line may be removed for the following QSOs in the section by simply inserting a line starting with `)`:
```
)
```
In the example just given, this would remove the `SOTA_REF` field, which would be equivalent to inserting a new QSO definition line like
```
= [0-9]{4} TIME_ON CALL RST_SENT RST_RCVD
```

### Printing control
`sf2adif` prints each ADIF field in a line by itself. Some logging programs do not like this. A line starting with the keyword `ONELINE` generates ADIF records with blank-separated fields.

### Comment lines
Lines starting with `#` are ignored by `sf2adif`.

### A complete example
Here's an example of a SOTA log:
```
& [0-9]{8} QSO_DATE STATION_CALLSIGN OPERATOR MY_SOTA_REF MY_GRIDSQUARE
@ (FM|SSB|USB|LSB|CW) MODE FREQ TX_PWR 
= [0-9]{4} TIME_ON CALL RST_SENT RST_RCVD
+ freq= FREQ out= RST_SENT in= RST_RCVD sota_ref= MY_SOTA_REF
20231118 EA5IYL EA5IYL EA5/CS-012 IN90vi Castell de Culla
CW 14.062 20 Xiegu G90 + 20-m-band whip + counterpoise
1042 IX1IHR 579 539
1044 EC8ADS 589 559
1045 OK2PDT 579 579
1047 EI6FR 589 559
1048 SM5LNE 599 549
1049 EA1PJ 589 589
1051 G4ZIB 599 599
1053 OE7PHI 589 559
1054 G4YTK 579 559
1056 DL1FU 599 569
1057 DL3NM 589 559 
( SOTA_REF
1100 SV2RUJ/P 579 ??? SV/TL-087
)
1102 HB9CGA 599 559
1104 2W0ILQ 579 446
1105 DL4ROB 579 559
1106 DJ2MX 589 579
1107 IN3NJB 579 559
( SOTA_REF
1110 IK2LEY/P 579 559 I/LO-422
)
1112 EA7GV 579 529
1115 G4YBU 579 559
1117 DL8DXL 589 559
1118 SV2NCH 589 449
USB 14.285 20 Xiegu G90 + 20-m-band whip + counterpoise
1121 M0DLL 59 55
1122 IN3ADF 59 57
1122 EA5FPL 59 59 
1123 HB9DHA 58 55
1124 HB9EAJ/P 55 55 S2S:HB/BL-004
1125 SP6KEP 57 55
1126 SA4BLM 58 53
FM 145.525 4 Yaesu FT-60 + NA-771
( GRIDSQUARE
1150 EA3ION 53 53 JN01la
)
```
This file is provided as `castelldeculla.sf` in the `examples` directory of the repository. 

## Separating definitions from content
If one writes more than one short-format log with the same format, it may be a good idea to put the definition lines for that short format in a separate *header* file such as ``sota.sfh`` and just put content in the log files (`castelldeculla.sf` in the example), and then use `sf2adif` as follows
```
gawk -f sf2adif.awk sota.sfh castelldeculla.sf >castelldeculla.adi
```

In the example above, `sota.sfh` could contain
```
& [0-9]{8} QSO_DATE STATION_CALLSIGN OPERATOR MY_SOTA_REF MY_GRIDSQUARE
@ (FM|SSB|USB|LSB|CW) MODE FREQ TX_PWR 
= [0-9]{4} TIME_ON CALL RST_SENT RST_RCVD
+ freq= FREQ out= RST_SENT in= RST_RCVD sota_ref= MY_SOTA_REF
```
as this is common to all SOTA logs in this format. The file `sota.sfh` is provided in the `examples` directory of the repository. 

### Definition changes on the spot?
`sf2adif` allows for definition lines to be put anywhere in the code, but the effect of this has not been fully tested. Use at your own risk.

### More than one log in a single short-format file?
Yes, you can have more than one log following the same definitions in a short-format file (that is, there may be more than a log-level content line), but `sf2adif` will generate a single ADIF log file from them.

## Roadmap
* Add functionality for satellite QSOs
* Further testing the possibility of changing definitions on the fly

## Contributing
Developers welcome! 

## Authors and acknowledgment
(c) Mikel Forcada EA5IYL, 2023–2024

## License
This project is licensed under the GNU General Public Licence v. 3.0.


