# (c) 2023-2024 Mikel Forcada EA5IYL
# Licence: GPL v3
# new short format (nsf) to ADIF

# The format is based on three types of lines.
# Lines starting with "@ <regular_expression>" such as
# @ [0-9]{8} QSO_DATE STATION_CALLSIGN OPERATOR MY_GRIDSQUARE TX_PWR 
#
# are section-level definition lines that define the format of section-level content lines (that do not start with 
# "@" or "=" or "+") such as
#
# 20230325 EA5IYL EA5IYL IM98ri 4 EFHW with 64:1
# 
# where 20230325 sets the value of QSO_DATE, 
# EA5IYL sets the value of STATION_CALLSIGN and OPERATOR,  
# IM98ri sets the value of MY_GRIDSQUARE, 
# 4 sets the value of TX_PWR and any trailing text ("EFHW with 64:1" 
# in this case) will be used for COMMENT and NOTES for any QSO after it.
#
# Lines starting with "= <regular_expression>" such as
#
# = [0-9]{1,2}[.][0-9]{3,5} FREQ CALL SRX STX TIME_ON 
#
# are QSO definition lines that define the format of QSO content lines (that do not start with "@" 
# or "=" or "+") such as
# 
# 28.5919	EA8TR	654 062	1508 amazing stuff
#
# as a QSO where FREQ=28.5919, CALL=EA8TR, SRX=654, STX=062, TIME=1508, 
# and the QSO comment the remainder of the line ("amazing stuff" in this 
# case) and writes an ADIF QSO with these ADIF fields and the ADIF 
# fields defined by earlier "@"-lines and with COMMENT and NOTES set to 
# the concatenation of the context comment and
# the QSO comment. 
#
# Lines starting with "+" define material that is added to the 
# QSO comment from the ADIF fields recognized; for instance,
# + freq= FREQ sin= SRX sout= STX
# will add to the comment of each QSO a string like
# " freq=28.5919 sin=654 sout=062"
# where 28.5919 is the current FREQ, 654 is the current SRX and 
# 062 is the current STX.
# This is useful to submit ADIF logs to those sites that accept 
# comments but ignore some ADIF fields.
#
# 
# That is, "@"-lines define the values of a set of ADIF fields that will 
# be shared by the following QSOs and "="-lines define the values of 
# another set of ADIF fields and writes an ADIF QSO with the parameters 
# defined by the last "@-line" and the current "="-line. 
# A comment constructed from the context comment, the QSO comment
# and any additional fields mentioned in the "+" line is appended to 
# both NOTES and COMMENTS.
#
# [Added support for "log-level" comments with &, similar to @, to be
# properly documented]
#
# 20231119 Adding support for ">" to change a context field directly in the context
# 20240101 Adding support to append fields and chop off fields from the QSO line definition 
# 20231120 Please refer to the README.md file in the repository for a better explanation.
#
#
# 
#
# Example:
# @ [0-9]{8} QSO_DATE STATION_CALLSIGN OPERATOR MY_GRIDSQUARE TX_PWR 
# 20230325 EA5IYL EA5IYL IM98ri 5 horizontal mag. loop
# = [0-9]{1,2}[.][0-9]{3,5} FREQ CALL SRX STX TIME_ON
# + freq= FREQ sin= SRX sout= STX
# 14.203 	IU4FNO 	347	001	1442
# 28.4003	ES9UKR	1216	058	1456
# 28.419	YL6W	075	059	 1459
# 28.536	LY4A	2728	060	1501
# 28.536	5B4KH	1488	061	1503
# 28.5919	EA8TR	654 062	1508
# 28.450	ES7A	1479	063 1516
# 28.409	UI2K	124	064	1517
# 28.427	KW8N	790 065	1529
# 28.295	CF3A	2727 066	1539 
# 28.3694	EC8AQQ	849	067	1543
# 28.457	LZ1ND	456	068	1547
# 28.4515	SP8R	4016	069	1549
# 28.489	LY4A	2830	070	1556
# 28.6294	LY4L	838	071	1559
# 28.518	LZ9W	851	072	1608
# 28.502	LZ5R	5127	073	1610 amazing!
# 28.530	UT0RS	018	074	1611
# 20230325 EA5IYL EA5IYL IM98ri 4 EFHW with 64:1
# 28.498	EB8AH	3005	076	1618
# 28.364	P33W	6890	077	1620
# 28.6473	4X6FR	2305	078	1637 wonderful!




BEGIN{
print "<ADIF_VER:5>3.0.4";
print "<PROGRAM_ID:15>sf2adif.awk";
print "<PROGRAMVERSION:4>0.99";
print "<EOH>";
print "";

recfor = "%s\n"   # each record in a new line, unless overwritten by ONELINE

qso_regex="DUMMY"; # avoid stray QSOs before any "=" line [I wonder why this is needed]

}

/^ONELINE/ { recfor = "%s " }

/^[&]/ {log_regex=$2; log_fields=NF-2;
        for(i=3; i<=NF; i++) log_name[i-2]=$i;
        }

/^@/ { context_regex=$2; context_fields=NF-2;
       for (i=3; i<=NF; i++) context_name[i-2]=$i;
     }
         
         
/^[#]/   { # do nothing
         }
         
/^=/  { qso_regex=$2; qso_fields=NF-2;
        for (i=3; i<=NF; i++) qso_name[i-2]=$i;
	  }
	  
/^[+]/  {  extended_comment_length=0;
           for (i=1;i<=int((NF-1)/2);i++)
            { extended_literal[i]=$(2*i);
              extended_field[i]=$(2*i+1);
              extended_comment_length++;
            }
        } 

# Change a specific context field by ADIF name until next context	                  
/^[>]/         {
                   context_value[$2]=$3;
               }	  
         
$1 ~ "^"log_regex"$" { for(i=1; i<=log_fields; i++)
		                      log_value[log_name[i]]=$i;
		                   log_comment="";
		                   for (i=1; i<=log_fields; i++) $i="";
		                   log_comment=substr($0,log_fields+1);
		                  }

$1 ~ "^"context_regex"$" { for(i=1; i<=context_fields; i++)
		                      context_value[context_name[i]]=$i;
		                   context_comment="";
		                   for (i=1; i<=context_fields; i++) $i="";
		                   context_comment=substr($0,context_fields+1);
		                  }
	
/^\(/ { qso_fields++; qso_name[qso_fields]=$2;}

/^\)/ { qso_fields--}

					
$1 ~ "^"qso_regex"$" { for(i=1; i<=context_fields; i++) 
                           printf recfor, "<" context_name[i] ":" length(context_value[context_name[i]]) ">" context_value[context_name[i]];
                       for(i=1; i<=log_fields; i++) 
                           printf recfor, "<" log_name[i] ":" length(log_value[log_name[i]]) ">" log_value[log_name[i]];
                       for(i=1; i<=qso_fields; i++)
                           { printf recfor, "<" qso_name[i] ":" length($i) ">" $i;
                             qso_value[qso_name[i]]=$i;
                           }
                       for(i=1; i<=qso_fields; i++) $i="";
                       qso_comment=substr($0,qso_fields+1);
                       comment=log_comment " " context_comment " " qso_comment;
                       gsub(/^[ ]/,"",comment);
                       gsub(/[ ]$/,"",comment);
                       if (extended_comment_length)
                            for (i=1; i<=extended_comment_length; i++)
                            {    comment=comment " " extended_literal[i];
                                if (extended_field[i] in qso_value) comment=comment qso_value[extended_field[i]];
                                if (extended_field[i] in log_value) comment=comment log_value[extended_field[i]];
                                if (extended_field[i] in context_value) comment=comment context_value[extended_field[i]];
                            }
                       gsub("(^[ ]|[ ]$|[ ][ ]+)","",comment)   
                       if (length(comment)!=0) {
                           printf recfor, "<NOTES:" length(comment) ">" comment;
                           printf recfor, "<COMMENT:" length(comment) ">" comment;
                         }
                       printf recfor, "<EOR>";
                       print "";
               }


